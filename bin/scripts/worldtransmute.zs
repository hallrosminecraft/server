import mods.projecte.PhiloStone;
import mods.projecte.KleinStar;

#PhiloStone.addWorldTransmutation(<minecraft:obsidian>, <minecraft:grass>);

PhiloStone.addWorldTransmutation(<minecraft:red_flower:1>, <minecraft:red_flower:0>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:2>, <minecraft:red_flower:1>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:3>, <minecraft:red_flower:2>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:4>, <minecraft:red_flower:3>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:5>, <minecraft:red_flower:4>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:6>, <minecraft:red_flower:5>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:7>, <minecraft:red_flower:6>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:8>, <minecraft:red_flower:7>);
PhiloStone.addWorldTransmutation(<minecraft:red_flower:0>, <minecraft:red_flower:8>);

PhiloStone.addWorldTransmutation(<minecraft:double_plant:3>, <minecraft:double_plant:2>);
PhiloStone.addWorldTransmutation(<minecraft:double_plant:0>, <minecraft:double_plant:3>);
PhiloStone.addWorldTransmutation(<minecraft:double_plant:1>, <minecraft:double_plant:0>);
PhiloStone.addWorldTransmutation(<minecraft:double_plant:4>, <minecraft:double_plant:1>);
PhiloStone.addWorldTransmutation(<minecraft:double_plant:5>, <minecraft:double_plant:4>);
PhiloStone.addWorldTransmutation(<minecraft:double_plant:2>, <minecraft:double_plant:5>);

PhiloStone.addWorldTransmutation(<minecraft:ice>, <minecraft:glass>);


PhiloStone.addWorldTransmutation(<minecraft:stone:1>, <minecraft:stone:3>);
PhiloStone.addWorldTransmutation(<minecraft:stone:5>, <minecraft:stone:1>);
PhiloStone.addWorldTransmutation(<minecraft:stone:3>, <minecraft:stone:5>);

furnace.addRecipe(<quark:marble>, <minecraft:stone:3>);

PhiloStone.addWorldTransmutation(<quark:limestone>, <quark:marble>);
PhiloStone.addWorldTransmutation(<quark:basalt>, <quark:limestone>);
PhiloStone.addWorldTransmutation(<quark:marble>, <quark:basalt>);


