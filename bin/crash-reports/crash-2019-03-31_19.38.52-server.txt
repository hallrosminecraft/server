---- Minecraft Crash Report ----

WARNING: coremods are present:
  Do not report to Forge! Remove FoamFixAPI (or replace with FoamFixAPI-Lawful) and try again. (foamfix-0.5.4-anarchy.jar)
  Default Options (DefaultOptions_1.10.2-6.1.5.jar)
  LoadingPlugin (Quark-r1.1-70.jar)
  EnderCorePlugin (EnderCore-1.10.2-0.4.1.66-beta.jar)
  Ar_CorePlugin (additionalresources-1.9.4-0.2.0.28+47cd0bd.jar)
  ChiselCorePlugin (Chisel-MC1.10.2-0.0.8.11.jar)
  ForgelinPlugin (Forgelin-1.3.1.jar)
  FMLPlugin (InventoryTweaks-1.61-58.jar)
  CCLCorePlugin (CodeChickenLib-1.10.2-2.5.4.218-universal.jar)
Contact their authors BEFORE contacting forge

// You should try our sister game, Minceraft!

Time: 2019-03-31 19:38
Description: Exception in server tick loop

Missing Mods:
	valkyrielib : [1.10.2-0.10.6,)

net.minecraftforge.fml.common.MissingModsException: Mod environmentaltech (Environmental Tech) requires [valkyrielib@[1.10.2-0.10.6,)]
	at net.minecraftforge.fml.common.Loader.sortModList(Loader.java:266)
	at net.minecraftforge.fml.common.Loader.loadMods(Loader.java:536)
	at net.minecraftforge.fml.server.FMLServerHandler.beginServerLoading(FMLServerHandler.java:98)
	at net.minecraftforge.fml.common.FMLCommonHandler.onServerStart(FMLCommonHandler.java:328)
	at net.minecraft.server.dedicated.DedicatedServer.func_71197_b(DedicatedServer.java:121)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:431)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.10.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_201, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 628246768 bytes (599 MB) / 978321408 bytes (933 MB) up to 978321408 bytes (933 MB)
	JVM Flags: 2 total; -Xmx1G -Xms1G
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.32 Powered by Forge 12.18.3.2511 70 mods loaded, 70 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	U	mcp{9.19} [Minecraft Coder Pack] (minecraft.jar) 
	U	FML{8.0.99.99} [Forge Mod Loader] (forge-1.10.2-12.18.3.2511-universal.jar) 
	U	Forge{12.18.3.2511} [Minecraft Forge] (forge-1.10.2-12.18.3.2511-universal.jar) 
	U	additionalresources{0.1.1} [Additional Resources] (additionalresources-1.9.4-0.2.0.28+47cd0bd.jar) 
	U	foamfixcore{7.7.4} [FoamFixCore] (minecraft.jar) 
	U	fenceoverhaul{1.2.1} [Fence Overhaul] ([1.10.x]FenceOverhaul-1.2.1.jar) 
	U	aether_legacy{v1.3.1-1.10.2} [Aether Legacy] (aether_legacy-1.3.1.jar) 
	U	AkashicTome{1.0-5} [Akashic Tome] (AkashicTome-1.0-5.jar) 
	U	AutoRegLib{1.0-2} [AutoRegLib] (AutoRegLib-1.0-2.jar) 
	U	Baubles{1.3.9} [Baubles] (Baubles-1.10.2-1.3.9.jar) 
	U	betterbuilderswands{0.10.2} [Better Builder's Wands] (BetterBuildersWands-1.10.2-0.10.2.209+115b204.jar) 
	U	Botania{r1.9-340} [Botania] (Botania r1.9-340.jar) 
	U	Chameleon{1.10-2.2.2} [Chameleon] (Chameleon-1.10-2.2.2.jar) 
	U	chisel{MC1.10.2-0.0.8.11} [Chisel] (Chisel-MC1.10.2-0.0.8.11.jar) 
	U	chiselsandbits{12.12} [Chisels & Bits] (chiselsandbits-12.12.jar) 
	U	CodeChickenCore{2.4.0.101} [CodeChicken Core] (CodeChickenCore-1.10.2-2.4.0.101-universal.jar) 
	U	CodeChickenLib{2.5.4.218} [CodeChicken Lib] (CodeChickenLib-1.10.2-2.5.4.218-universal.jar) 
	U	ccl-entityhook{1.0} [ccl-entityhook] (CodeChickenLib-1.10.2-2.5.4.218-universal.jar) 
	U	MineTweaker3{3.0.20} [MineTweaker 3] (CraftTweaker-1.10.2-3.0.20.jar) 
	U	crafttweakerjei{1.0.1} [CraftTweaker JEI Support] (CraftTweaker-1.10.2-3.0.20.jar) 
	U	dimensionalcake{0.0.1} [Dimensional Cake] (dimensionalcake-1.9.4-0.0.1.jar) 
	U	endercore{1.10.2-0.4.1.66-beta} [EnderCore] (EnderCore-1.10.2-0.4.1.66-beta.jar) 
	U	EnderIO{1.10.2-3.1.193} [Ender IO] (EnderIO-1.10.2-3.1.193.jar) 
	U	EnderStorage{2.2.1.101} [EnderStorage] (EnderStorage-1.10.2-2.2.1.101-universal.jar) 
	U	energyconverters{1.0.0.26} [Energy Converters] (energyconverters_1.10.2-1.0.0.26.jar) 
	U	engineersworkshop{1.3.6-1.10.2} [Engineer's Workshop] (EngineersWorkshop-1.3.7-1.10.2.jar) 
	U	environmentaltech{1.10.2-0.10.6b} [Environmental Tech] (environmentaltech-1.10.2-0.10.6b.jar) 
	U	excompressum{2.0.97} [Ex Compressum] (ExCompressum_1.10.2-2.0.97.jar) 
	U	exnihiloadscensio{0.1.5} [Ex Nihilo Adscensio] (exnihiloadscensio-1.10.2-0.1.20.jar) 
	U	extrautils2{1.0} [Extra Utilities 2] (extrautils2-1.10.2-1.3.0.jar) 
	U	bigreactors{1.10.2-0.4.5.49} [Extreme Reactors] (ExtremeReactors-1.10.2-0.4.5.49.jar) 
	U	fairylights{2.1.3} [Fairy Lights] (fairylights-2.1.3-1.10.2.jar) 
	U	fastleafdecay{v11} [Fast Leaf Decay] (FastLeafDecay-v11.jar) 
	U	flatcoloredblocks{mc1.10-v4.4} [Flat Colored Blocks] (flatcoloredblocks-mc1.10-v4.4.jar) 
	U	fluxnetworks{1.3.9} [Flux Networks] (fluxnetworks-1.10.2-1.2.9.jar) 
	U	foamfix{@VERSION@} [FoamFix] (foamfix-0.5.4-anarchy.jar) 
	U	forgelin{1.3.0} [Forgelin] (Forgelin-1.3.1.jar) 
	U	ftbl{0.0.0} [FTBLib] (FTBLib-1.10.2-3.3.0.jar) 
	U	ftbu{0.0.0} [FTBUtilities] (FTBUtilities-1.10.2-3.3.1.jar) 
	U	funkylocomotion{1.0} [Funky Locomotion] (funky-locomotion-1.10.2-alpha-0.0.3.jar) 
	U	gravestone{1.5.12} [Gravestone] (GraveStone Mod 1.5.12.jar) 
	U	immersiveengineering{0.10-56} [Immersive Engineering] (ImmersiveEngineering-0.10-56.jar) 
	U	inventorytweaks{1.61-58-a1fd884} [Inventory Tweaks] (InventoryTweaks-1.61-58.jar) 
	U	JEI{3.14.5.406} [Just Enough Items] (jei_1.10.2-3.14.5.406.jar) 
	U	librarianliblate{1.10.1} [LibrarianLib Stage 2] (librarianlib-1.10.1.jar) 
	U	librarianlib{1.10.1} [LibrarianLib] (librarianlib-1.10.1.jar) 
	U	mcmultipart{1.4.0} [MCMultiPart] (MCMultiPart-1.4.0-universal.jar) 
	U	modtweaker{2.0.9} [Mod Tweaker] (ModTweaker2-2.0.9.jar) 
	U	mtlib{@VERSION@} [MTLib] (MTLib-1.0.1.jar) 
	U	mxtune{0.3.1.0-dev.16} [mxTune] (mxtune-1.10.2-0.3.1.0-dev.16.jar) 
	U	ProjectE{1.10.2-PE1.2.0} [ProjectE] (ProjectE-1.10.2-PE1.2.0.jar) 
	U	Psi{r1.0-42} [Psi] (Psi-r1.0-42.jar) 
	U	Quark{r1.1-70} [Quark] (Quark-r1.1-70.jar) 
	U	refraction{1.3.4} [Refraction] (refraction-1.3.4.jar) 
	U	rftools{5.84} [RFTools] (rftools-1.1x-5.84.jar) 
	U	shadowmc{3.6.1} [ShadowMC] (ShadowMC-1.10.2-3.6.1.jar) 
	U	SleepingBag{1.4.0} [Sleeping Bag] (SleepingBag-1.10.2-1.4.0.jar) 
	U	sonarcore{3.3.1} [SonarCore] (sonarcore-1.10.2-3.3.1.jar) 
	U	StorageDrawers{1.10.2-3.5.17} [Storage Drawers] (StorageDrawers-1.10.2-3.5.17.jar) 
	U	rscircuits{1.0.4} [Super Circuit Maker] (SuperCircuitMaker-1.0.4.jar) 
	U	theoneprobe{1.4.2} [The One Probe] (theoneprobe-1.1x-1.4.2.jar) 
	U	torchmaster{1.0} [TorchMaster] (torchmaster_1.10.2-1.1.0.11.jar) 
	U	Translocator{2.1.3.52} [Translocator] (Translocators-1.10.2-2.1.3.52-universal.jar) 
	U	treegrowingsimulator{0.0.4} [Tree Growing Simulator 2016] (TreeGrowingSimulator2016-0.0.4.jar) 
	U	VeinMiner{0.35.3_1.9-a46c1b0} [Vein Miner] (VeinMiner-1.9-0.35.3.595+a46c1b0.jar) 
	U	VeinMinerModSupport{0.35.3_1.9-a46c1b0} [Mod Support] (VeinMiner-1.9-0.35.3.595+a46c1b0.jar) 
	U	villagebox{0.6.0} [Village Box] (villagebox-1.10.2-0.6.0.jar) 
	U	voidislandcontrol{1.1.2} [Void Island Control] (voidislandcontrol-1.1.2.jar) 
	U	xprings{1.3.0} [Experience Rings] (xprings-1.10-1.3.0.jar) 
	U	zerocore{1.10.2-0.1.2.2} [Zero CORE] (zerocore-1.10.2-0.1.2.2.jar) 
	Loaded coremods (and transformers): 
Do not report to Forge! Remove FoamFixAPI (or replace with FoamFixAPI-Lawful) and try again. (foamfix-0.5.4-anarchy.jar)
  pl.asie.foamfix.coremod.FoamFixTransformer
Default Options (DefaultOptions_1.10.2-6.1.5.jar)
  net.blay09.mods.defaultoptions.coremod.DefaultOptionsClassTransformer
LoadingPlugin (Quark-r1.1-70.jar)
  vazkii.quark.base.asm.ClassTransformer
EnderCorePlugin (EnderCore-1.10.2-0.4.1.66-beta.jar)
  com.enderio.core.common.transform.EnderCoreTransformer
Ar_CorePlugin (additionalresources-1.9.4-0.2.0.28+47cd0bd.jar)
  
ChiselCorePlugin (Chisel-MC1.10.2-0.0.8.11.jar)
  team.chisel.common.asm.ChiselTransformer
ForgelinPlugin (Forgelin-1.3.1.jar)
  
FMLPlugin (InventoryTweaks-1.61-58.jar)
  invtweaks.forge.asm.ContainerTransformer
CCLCorePlugin (CodeChickenLib-1.10.2-2.5.4.218-universal.jar)
  codechicken.lib.asm.ClassHeirachyManager
  codechicken.lib.asm.CCL_ASMTransformer
	Profiler Position: N/A (disabled)
	Is Modded: Definitely; Server brand changed to 'fml,forge'
	Type: Dedicated Server (map_server.txt)